Distributable django app generator
==================================

Simple way to create a new distributable django app.

Can be adapted to work with pure python packages, as well.


Creating a new distributable app
================================

Run ``startapp.sh <app_name>`` to start a new app.

That's it. Your app is ready for testing and development.


Testing your app
----------------

To test your new app you can run::
    
    <app_name>/run_tests.sh


Testing customization
---------------------

In order to customize your testing experience you can edit the following files:

* <app\_name>/testconf/settings.py
* <app\_name>/testconf/settings\_shell.py
* <app\_name>/testconf/urls.py
* <app\_name>/run\_tests.sh


Distribution configuration
--------------------------

To fully configure your app for distribution you should edit:

* config.py


For a more advanced configuration you can also edit:

* setup.py

